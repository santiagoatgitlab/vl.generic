package com.victorialand.vlGeneric;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;


/**
 * Created by Santiago on 02/08/2016.
 */
public class Assets {

    public static FreeTypeFontGenerator verdana;
    public static Texture splashImage;
    public static Texture victorialand;
    public static Texture[] character;

}
