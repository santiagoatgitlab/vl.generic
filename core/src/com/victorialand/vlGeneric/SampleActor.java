package com.victorialand.vlGeneric;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.victorialand.vlGeneric.vlf.GameConfig;
import com.victorialand.vlGeneric.vlf.configuration.Config;

/**
 * Created by root on 13/07/17.
 */

public class SampleActor extends Actor{

    Sprite images[];
    int currentImage;
    float accTime;

    public SampleActor(){

        images = new Sprite[]{
            new Sprite(Assets.character[0]),
            new Sprite(Assets.character[1]),
            new Sprite(Assets.character[2]),
            new Sprite(Assets.character[1]),
        };

        setSize(Config.ssm.width * 0.2f * 0.61f,Config.ssm.width * 0.2f);
        setPosition(Config.ssm.width/2 - getWidth()/2, Config.ssm.height/2  + GameConfig.planetSize/2);
        setOrigin(getWidth()/2, -GameConfig.planetSize/2);


    }

    @Override
    public void act(float deltaTime){

        accTime += deltaTime * 10;

        if (accTime >= images.length ){
            accTime -= images.length;
        }

        currentImage = (int)(Math.floor(accTime)) % images.length;
        rotateBy(-45*deltaTime);


    }

    @Override
    public void draw(Batch batch, float alpha){

        batch.draw(images[currentImage],getX(),getY(),getOriginX(),getOriginY(),getWidth(),getHeight(),getScaleX(),getScaleY(),getRotation());

    }

}
