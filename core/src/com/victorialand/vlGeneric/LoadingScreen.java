package com.victorialand.vlGeneric;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.victorialand.vlGeneric.vlf.configuration.Config;
import com.victorialand.vlGeneric.vlf.utils.Debugger;
import com.victorialand.vlGeneric.vlf.utils.ImageRect;
import com.victorialand.vlGeneric.vlf.utils.Logger;
import com.victorialand.vlGeneric.vlf.utils.VL;

/**
 * Created by Santiago on 02/08/2016.
 */
public class LoadingScreen extends ScreenAdapter {


    SpriteBatch batch;
    float time;
    ImageRect splash;

    @Override
    public void show(){

        batch = new SpriteBatch();

        Assets.splashImage          = new Texture(Gdx.files.internal("data/splash.png"));
        Assets.victorialand         = new Texture(Gdx.files.internal("data/victorialand.png"));
        Assets.verdana              = new FreeTypeFontGenerator(Gdx.files.internal("fonts/verdana.ttf"));
        Assets.character            = new Texture[]{
                                        new Texture(Gdx.files.internal("data/imagen.png")),
                                        new Texture(Gdx.files.internal("data/imagen2.png")),
                                        new Texture(Gdx.files.internal("data/imagen3.png"))
                                    };

        VL.fontParameter.size       = Math.round(30 * Config.ssm.referenceWidthFactor);
        Main.debugFont              = Assets.verdana.generateFont(VL.fontParameter);
        Main.FMFont                 = Assets.verdana.generateFont(VL.fontParameter);


        Main.debugger               = new Debugger();
        Main.logger                 = new Logger("main_log.dat");

        splash = new ImageRect(Assets.splashImage,0,0,Config.ssm.width, Config.ssm.height, "splash");
        splash.setX(Config.ssm.getFixedBackgroundX());

    }

    @Override
    public void render(float deltaTime){

        time += deltaTime;
        if (time > 1f){
            Config.game.setScreen(new GameScreen());
        }
        Gdx.gl.glClearColor(1,1,1,1); //39 98 161
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        batch.begin();
        splash.draw(batch, 1);
        batch.end();

    }


    @Override
    public void dispose(){

        batch.dispose();

    }
}
