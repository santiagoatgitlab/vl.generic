package com.victorialand.vlGeneric;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.victorialand.vlGeneric.vlf.GameConfig;
import com.victorialand.vlGeneric.vlf.configuration.Config;
import com.victorialand.vlGeneric.vlf.utils.ImageRect;


/**
 * Created by Santiago on 18/09/2016.
 */

public class GameScreen extends ScreenAdapter implements InputProcessor{

    public static int scrollY;
    Stage stage;
    SampleActor character;
    ImageRect victorialand;


    @Override
    public void show () {


        //batch = new SpriteBatch();
        stage = new Stage();
        character = new SampleActor();
        victorialand = new ImageRect(Assets.victorialand, 0,0,0,0,"0");
        victorialand.setSize(GameConfig.planetSize,GameConfig.planetSize);
        victorialand.setPosition(Config.ssm.width/2-victorialand.getWidth()/2,Config.ssm.height/2-victorialand.getHeight()/2);

        Main.debugger.setColor(Color.BLACK);
        Main.debugger.set("asdasd","asdasd");

        stage.addActor(Main.debugger);
        stage.addActor(character);
        stage.addActor(victorialand);

        Gdx.input.setInputProcessor(this);

    }

    public void update(float deltaTime){

        //
        //update, se ejecuta toodo el tiempo ¿viste?
        //

        stage.act(deltaTime);

    }

    @Override
    public void render (float deltaTime) {
        update(deltaTime);
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
    }


    @Override
    public void dispose () {
        //batch.dispose();
        stage.dispose();
    }

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.BACK || keycode == Input.Keys.ESCAPE){
                Gdx.app.exit();
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) { return false; }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

}
