package com.victorialand.vlGeneric.vlf.configuration;

/**
 * Created by Santiago on 02/08/2016.
 */
public class Positions {

    public Position puntajeLabel = new Position(0.50f,0.91f);
    public Position puntajeMaximoLabel = new Position(0.5f,0.06f);
    public Position tiempo = new Position(0.79f,0.91f);
    public Position masDos = new Position(0.79f,0.89f);
    public Position matches = new Position(0.50f, 0.93f);
    public Position textTap = new Position(0.50f, 0.38f);

}
