package com.victorialand.vlGeneric.vlf.configuration;

/**
 * Created by Santiago on 26/07/2016.
 */
public enum SizeBehavior {

    ALIGN_CENTER, STRETCH

}
