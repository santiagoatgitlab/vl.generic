package com.victorialand.vlGeneric.vlf.configuration;


/**
 * Created by Santiago on 26/07/2016.
 */
public class Sizes {

    public Size figura = new Size(0.33f,1f);
    public Size tiempo = new Size(0.1f,1f);
    public Size labelMasDos = new Size(0.1f, 0.1f);
    public Size smallButton = new Size(0.15f, 1f);

}
