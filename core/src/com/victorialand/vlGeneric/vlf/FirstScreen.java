package com.victorialand.vlGeneric.vlf;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Texture;
import com.victorialand.vlGeneric.Assets;
import com.victorialand.vlGeneric.LoadingScreen;
import com.victorialand.vlGeneric.vlf.configuration.Config;

/**
 * Created by Santiago on 02/08/2016.
 */
public class FirstScreen extends ScreenAdapter {

    @Override
    public void show(){

        String sizeDir = Config.ssm.sizeDir;
        Config.game.setScreen(new LoadingScreen());

    }
}
