package com.victorialand.vlGeneric;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.victorialand.vlGeneric.vlf.FirstScreen;
import com.victorialand.vlGeneric.vlf.configuration.Config;
import com.victorialand.vlGeneric.vlf.configuration.Orientation;
import com.victorialand.vlGeneric.vlf.configuration.Positions;
import com.victorialand.vlGeneric.vlf.configuration.ScreenSizeManager;
import com.victorialand.vlGeneric.vlf.configuration.SizeBehavior;
import com.victorialand.vlGeneric.vlf.configuration.Sizes;
import com.victorialand.vlGeneric.vlf.utils.Debugger;
import com.victorialand.vlGeneric.vlf.utils.Logger;


import java.util.Random;

public class Main extends Game {

	public static BitmapFont debugFont, FMFont;
	public static Debugger debugger;
	public static Logger logger;
	public static Preferences preferences;
	public static Random randomGenerator;
	public static int level;

	@Override
	public void create() {
		Config.game = this;
		Config.ssm = new ScreenSizeManager(SizeBehavior.STRETCH, Orientation.PORTRAIT);
		Config.sizes = new Sizes();
		Config.positions = new Positions();
		randomGenerator = new Random();
		preferences = Gdx.app.getPreferences("generic");


		setScreen(new FirstScreen());
	}
}
