package com.victorialand.vlGeneric.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.victorialand.vlGeneric.Main;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title = "Numeros";
		config.useGL30 = true;
		config.height = 512;
		config.width = 288;

		new LwjglApplication(new Main(), config);
	}
}
